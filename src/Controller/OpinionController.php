<?php

namespace App\Controller;

use App\Entity\opinion;
use App\Form\OpinionType;
use App\Controller\DemoController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class OpinionController extends AbstractController
{
    //Formulario dejar comentario

    /**
     * @Route("/maleteo/opiniones", name="opiniones")
     */
    public function dejarOpinion(Request $request, EntityManagerInterface $doctrine)
    {

        //form
        $form = $this->createForm(OpinionType::class);
        $form->handleRequest($request);
        $opinion = $form->getData();

        $repe = 0;

        if ($form->isSubmitted() && $form->isvalid()) {

            $doctrine->persist($opinion);
            $doctrine->flush();

            //$this->addFlash('success', 'Tu opinión ha sido guardada');
            return $this->redirectToRoute("maleteo");
        }
        return $this->render(
            'forms/opinionForm.html.twig',
            ['opinionForm' => $form->createView()]
        );
    }
}
