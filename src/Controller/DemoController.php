<?php

namespace App\Controller;

use App\Entity\Opinion;
use App\Entity\Solicitud;
use App\Form\SolicitudType;
use App\Form\OpinionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DemoController extends AbstractController
{

    /**
     * @Route("/maleteo", name="maleteo")
     */
    public function homePage(Request $request, EntityManagerInterface $doctrine)
    {
        //form

        $form = $this->createForm(SolicitudType::class);
        $form->handleRequest($request);
        $solicitud = $form->getData();

        $repe = 0;

        if ($form->isSubmitted() && $form->isvalid()) {
            $repositorioSol = $doctrine->getRepository(Solicitud::class);
            $solicitudes = $repositorioSol->findAll();

            foreach ($solicitudes as $sol) {
                if ($solicitud->getEmail() == $sol->getEmail()) {
                    $repe++;
                }
            }

            if ($repe != 0) {
                $this->addFlash('failure', 'Ya has realizado una solicitud previamente');
            } else {

                $doctrine->persist($solicitud);
                $doctrine->flush();

                unset($solicitud);
                unset($form);
                $solicitud = new Solicitud();
                $form = $this->createForm(SolicitudType::class);
                $this->addFlash('success', 'Tu solicitud ha sido realizada correctamente');
                //return $this->redirectToRoute("maleteo");
            }
        }

        //Opiniones

        $repositorioOpinion = $doctrine->getRepository(Opinion::class);
        $opiniones = $repositorioOpinion->findAll();
        shuffle($opiniones);

        return $this->render(
            'homePage.html.twig',
            ['solicitudForm' => $form->createView(), 'opiniones' => $opiniones]
        );
    }
}
