<?php

namespace App\Controller;

use App\Entity\Opinion;
use App\Entity\Solicitud;
use App\Form\SolicitudType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SolicitudesController extends AbstractController
{

    /**
     * @Route("/maleteo/solicitudes", name="solicitudes")
     */
    public function verSolicitudes(Request $request, EntityManagerInterface $doctrine)
    {
        $repositorioSol = $doctrine->getRepository(Solicitud::class);
        $solicitudes = $repositorioSol->findAll();

        return $this->render("solicitud/listadoSolicitudes.html.twig", ['solicitudes' => $solicitudes]);
    }
}
