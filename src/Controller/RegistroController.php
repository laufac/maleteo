<?php

namespace App\Controller;

use App\Form\UserRegistroType;
use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistroController extends AbstractController
{

    /**
     * @Route("/maleteo/registro", name="registro")
     */

    public function registro(Request $request, EntityManagerInterface $doctrine, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guard, LoginFormAuthenticator $formAuthenticator)
    {
        $user = new User();

        $form = $this->createForm(UserRegistroType::class);
        $form->handleRequest($request);
        $registro = $form->getData();

        $repe = 0;

        if ($form->isSubmitted() && $form->isvalid()) {
            $repositorioUser = $doctrine->getRepository(User::class);
            $users = $repositorioUser->findAll();

            foreach ($users as $user) {
                if ($user->getEmail() == $registro->getEmail()) {
                    $repe++;
                }
            }

            if ($repe != 0) {
                $this->addFlash('failure', 'Ya estás registrado con esta cuenta');
            } else {

                $password = $registro->getPassword();
                $registro->setPassword($passwordEncoder->encodePassword($user, $password));
                $doctrine->persist($registro);
                $doctrine->flush();

                $this->addFlash('success', 'Te has registrado correctamente');

                return $guard->authenticateUserAndHandleSuccess($registro, $request, $formAuthenticator, 'main');
            }
        }
        return $this->render(
            'forms/registroForm.html.twig',
            ['registroForm' => $form->createView()]
        );
    }
}
