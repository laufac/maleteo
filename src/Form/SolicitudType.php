<?php

namespace App\Form;

use App\Entity\Solicitud;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SolicitudType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('email')
            ->add(
                'ciudad',
                ChoiceType::class,
                [
                    'choices' => [
                        'Barcelona' => 'barcelona',
                        'Bilbao' => 'bilbao',
                        'Las Palmas' => 'palmas',
                        'Madrid' => 'madrid',
                        'Málaga' => 'malaga',
                        'Murcia' => 'murcia',
                        'Sevilla' => 'sevilla',
                        'Valencia' => 'valencia',
                        'Zaragoza' => 'zaragoza'
                    ]
                ]
            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Solicitud::class,
        ]);
    }
}
